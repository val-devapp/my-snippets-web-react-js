import "./App.css"
import Home from "./components/Home"
import Forms from "./components/Forms"
import { useSelector } from "react-redux"

function App() {

    const isLogged = useSelector((state) => state.authReducer)

    return (
        <div>
            {isLogged ? <Home/> : <Forms/>}
        </div>
    )
}
export default App
